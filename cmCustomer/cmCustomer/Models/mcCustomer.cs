namespace cmCustomer.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class mcCustomer
    {
        [Key]
        public int CustomerID { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Name")]
        public string CustomerName { get; set; }

        [StringLength(255)]
        [DisplayName("Address")]
        public string CustomerAddress { get; set; }

        [StringLength(255)]
        public string Remarks { get; set; }
    }
}
