﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cmCustomer.Models;

namespace cmCustomer.Controllers
{
    public class mcCustomersController : Controller
    {
        private DataContext db = new DataContext();

        // GET: mcCustomers
        public ActionResult Index(String name)
        {
            return View(db.mcCustomers.Where(x => x.CustomerName.StartsWith(name)|| name ==null).ToList());
        }

        // GET: mcCustomers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mcCustomer mcCustomer = db.mcCustomers.Find(id);
            if (mcCustomer == null)
            {
                return HttpNotFound();
            }
            return View(mcCustomer);
        }

        // GET: mcCustomers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: mcCustomers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CustomerID,CustomerName,CustomerAddress,Remarks")] mcCustomer mcCustomer)
        {
            if (ModelState.IsValid)
            {
                db.mcCustomers.Add(mcCustomer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mcCustomer);
        }

        // GET: mcCustomers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mcCustomer mcCustomer = db.mcCustomers.Find(id);
            if (mcCustomer == null)
            {
                return HttpNotFound();
            }
            return View(mcCustomer);
        }

        // POST: mcCustomers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CustomerID,CustomerName,CustomerAddress,Remarks")] mcCustomer mcCustomer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mcCustomer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mcCustomer);
        }

        // GET: mcCustomers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mcCustomer mcCustomer = db.mcCustomers.Find(id);
            if (mcCustomer == null)
            {
                return HttpNotFound();
            }
            return View(mcCustomer);
        }

        // POST: mcCustomers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            mcCustomer mcCustomer = db.mcCustomers.Find(id);
            db.mcCustomers.Remove(mcCustomer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
