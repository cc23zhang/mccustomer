﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cmCustomer.Startup))]
namespace cmCustomer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
